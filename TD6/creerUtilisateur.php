<!DOCTYPE html>
<html>
<head>
    <title> Exercice 3 </title>
    <meta charset="utf-8" />
</head>

<body>
<?php //echo var_dump($_GET)
use App\Covoiturage\Modele\Utilisateur;

require_once "Utilisateur.php";
$utilisateur = new Utilisateur($_POST['login'], $_POST['nom'], $_POST['prenom']);
$utilisateur->ajouter();
echo "L'utilisateur " . $utilisateur->getLogin() . " a bien été ajouté";
?>
</body>
</html>