<?php
/** @var Trajet $trajet */

use App\Covoiturage\Modele\DataObject\Trajet;

$idHTML = htmlspecialchars($trajet->getId());
$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";
$passagersStr = "";
echo "<p>
        Le trajet $nonFumeur du {$trajet->getDate()->format("d/m/Y")} partira de {$trajet->getDepart()} pour aller à {$trajet->getArrivee()}. 
        Le conducteur est : {$trajet->getConducteur()->getPrenom()} {$trajet->getConducteur()->getNom()}. </p>";
/**if (!empty($trajet->getPassagers())) {
    $passagersStr = implode(", ", $trajet->getPassagers());
} else {
    $passagersStr = "Aucun passager";
}*/

