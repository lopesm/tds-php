<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

echo "<h1>Liste des Trajets</h1>";
echo "<ol>";
foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    echo '<li><p> Trajet n° ' . $idHTML. " <a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&trajetId=$idHTML&controleur=trajet'>(= modif)</a> 
                                            <a href='controleurFrontal.php?action=afficherDetail&trajetId=$idHTML&controleur=trajet'>(+ d'info)</a> 
                                            <a href='controleurFrontal.php?action=supprimerTrajet&trajetId=$idHTML&controleur=trajet'>(- supp)</a></p> </li>";
}
echo "</ol>";
echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet'>Créer un trajet</a>";
?>