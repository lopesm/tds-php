<?php
/** @var string $trajet */

use App\Covoiturage\Modele\Repository\TrajetRepository;
$trajetO = (new TrajetRepository())->recupererParClePrimaire($trajet);
$departHTML = htmlspecialchars($trajetO->getDepart());
$arriveeHTML = htmlspecialchars($trajetO->getArrivee());
$dateHTML = htmlspecialchars($trajetO->getDate()->format('Y-m-d'));
$prixHTML = htmlspecialchars($trajetO->getPrix());
$loginHTML = htmlspecialchars($trajetO->getConducteur()->getLogin());
?>
<form method="get" action="controleurFrontal.php">
    <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="depart_id">Depart</label> :
            <input type="text" name="depart" id="depart_id" value="<?=$departHTML?>" required />
        </p>
        <p>
            <label for="arrivee_id">Arrivée</label> :
            <input type="text" placeholder="Sète" name="arrivee" id="arrivee_id" value="<?=$arriveeHTML?>" required />
        </p>
        <p>
            <label for="date_id">Date</label> :
            <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id" value="<?=$dateHTML?>" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="int" placeholder="20" name="prix" id="prix_id" value="<?=$prixHTML?>" required/>
        </p>
        <p>
            <label for="conducteurLogin_id">Login du conducteur</label> :
            <input type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" value="<?=$loginHTML?>" required/>
        </p>
        <p>
            <label for="nonFumeur_id">Non Fumeur ?</label> :
            <input type="checkbox" placeholder="leblancj" name="nonFumeur" id="nonFumeur_id" <?php if ($trajetO->isNonFumeur()) echo 'checked'; ?>/>
        </p>

        <p>
            <input type="submit" value="Modifier" />
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='trajet'>
            <input type="hidden" name="trajetId" value="<?=$trajet?>">

        </p>
    </fieldset>
</form>