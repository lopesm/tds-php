<?php
use App\Covoiturage\Lib\PreferenceControleur;

$userCoche = PreferenceControleur::existe() && PreferenceControleur::lire() == "utilisateur";
$trajetCoche = PreferenceControleur::existe() && PreferenceControleur::lire() == "trajet";
?><form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php echo ($userCoche ? "checked" : ""); ?>>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php echo ($trajetCoche ? "checked" : ""); ?>>
        <label for="trajetId">Trajet</label>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='enregistrerPreference'>
        </p>
    </fieldset>
</form>
