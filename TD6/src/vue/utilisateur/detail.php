<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Modele\Utilisateur;
use App\Covoiturage\Lib\ConnexionUtilisateur;

$nomHTML = htmlspecialchars($utilisateur->getNom());
    $prenomHTML = htmlspecialchars($utilisateur->getPrenom());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurldecode($utilisateur->getLogin());
    $loginURLEncode = rawurlencode($loginURL);
    echo '<p> Utilisateur de login ' . $loginHTML . '.</p>';
    echo "nom : $nomHTML  <br> prenom : $prenomHTML <br> login : $loginHTML<br> ";
    if(ConnexionUtilisateur::estUtilisateur($loginURL)) {
        echo "<a href='controleurFrontal.php?action=afficherFormulaireMiseAJour&login=$loginURLEncode'>Modifier les informations</a>";
        echo " <a href='controleurFrontal.php?action=supprimerUtilisateur&login=$loginURLEncode'>Supprimer le compte</a>";
    }
?>

