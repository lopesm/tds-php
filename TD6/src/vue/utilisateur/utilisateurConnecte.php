<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */
$loginHtml = htmlspecialchars($utilisateur->getLogin());
echo "<p>L'utilisateur $loginHtml est connecté !</p>";

require 'detail.php';