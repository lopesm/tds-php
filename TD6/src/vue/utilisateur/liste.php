<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Lib\ConnexionUtilisateur;

echo "<h1>Liste des utilisateurs</h1>";
echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin(), ENT_QUOTES);
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<li><p>Utilisateur de login ' . $loginHTML . ' ';
    echo " <a href='controleurFrontal.php?action=afficherDetail&login=$loginURL'>(+ d'info)</a>";


    echo '</p></li>';
}
echo "</ol>";
echo "<a href='controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur'>Créer un utilisateur</a>";
?>