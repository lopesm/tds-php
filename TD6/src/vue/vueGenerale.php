<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */

        use App\Covoiturage\Lib\MotDePasse;
        use App\Covoiturage\Lib\ConnexionUtilisateur;

        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/CSSminimaliste.css">
</head>
<body>
<header>
    <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                </li><li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                </li>

                <?php
                if(!ConnexionUtilisateur::estConnecte()) {
                    echo '
               <li>
                    <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/heart.png"></a>
                </li>
              
                
                <li >
                    <a href = "controleurFrontal.php?action=afficherFormulaireConnexion" ><img src = "../ressources/enter.png" ></a >
                </li >';
                }
                echo '<li>
                    <a href = "controleurFrontal.php?action=afficherFormulaireCreation" ><img src = "../ressources/add-user.png" ></a >
                    </li >';

                if(ConnexionUtilisateur::estConnecte()) {
                    $login = rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte());

                    echo'
                    <li>
                    <a href="controleurFrontal.php?action=afficherDetail&login='. $login .'"><img src="../ressources/user.png"></a>
                    </li>
                    
                    <li >
                    <a href = "controleurFrontal.php?action=afficherDeconnexion" ><img src = "../ressources/enter.png" ></a >
                </li >';
                    }
                    ?>

            </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Manon Lopes
    </p>

</footer>
</body>
</html>

