<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    public static int $dureeExpirationSession = 900;

    public static function getDureeExpirationSession(): int
    {
        return self::$dureeExpirationSession;
    }


}