<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php", "trajets" => $trajets]);  //"redirige" vers la vue
    }



    public static function afficherErreur(string $messageErreur): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    public static function afficherDetail() : void {
        if (isset($_GET['trajetId'])) {
            $id = $_GET['trajetId'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
            if (!isset($trajet)) {
                self::afficherErreur("Le trajet n'existe pas.");
            }
            else
                self::afficherVue('vueGenerale.php', ["titre" => "Détail de $id", "cheminCorpsVue" => "trajet/detail.php", "trajet" => $trajet]);
        }
        else
            self::afficherErreur("Le trajet n'a pas été renseigné ! ");

    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Création", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Trajet Créé","trajets" => $trajets, "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    public static function supprimerTrajet() : void {
        $id = $_GET['trajetId'];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Trajet Supprimé", "trajet" => $id, "trajets" => $trajets, "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }

    public static function mettreAJour() : void {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Trajet mis à jour", "trajets" => $trajets, "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
    }
    public static function afficherFormulaireMiseAJour() : void {
        $trajet = $_GET['trajetId'];
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Mise à jour trajet", "trajet" => $trajet, "trajets" => $trajets, "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }


    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["trajetId"] ?? null;
        return new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'],
            new \DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['prix'],
            (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']), isset($tableauDonneesFormulaire['nonFumeur']));

    }


}