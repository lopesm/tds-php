<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\vue\preferenceEnregistree;

class ControleurGenerique{

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Préférence Controleur", "cheminCorpsVue" => "utilisateur/formulairePreference.php"]);
    }
    public static function enregistrerPreference() : void
    {
        if (isset($_GET['controleur_defaut'])) {
            $preference = $_GET['controleur_defaut'];

            PreferenceControleur::enregistrer($preference);
            self:self::afficherVue('vueGenerale.php',["titre"=>"Préférence enregistrée", "cheminCorpsVue" => "preferenceEnregistree.php"]);
        } else {
            echo "Erreur : Aucune préférence de contrôleur n'a été envoyée.";
        }
    }
}
