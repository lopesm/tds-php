<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if(isset($_GET["login"])){
            $user = rawurldecode($_GET['login']);
        }else{
            $user = "";
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($user);
        if ($utilisateur == null) {
            /*require ('../vue/utilisateur/erreur.php');*/
            self::afficherErreur("L'utilisateur n'existe pas");
        }else{
            /*require ('../vue/utilisateur/detail.php');*/
            self::afficherVue("../vue/vueGenerale.php",["titre" => "Détails sur l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
        }
    }


    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $user = self::construireDepuisFormulaire($_GET);
        if($_GET["mdp"]==$_GET["mdp2"]){
            (new UtilisateurRepository())->ajouter($user);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Créé", "utilisateur" => $user->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        } else {
            self::afficherErreur("Mots de passe distincts.");
        }


    }

    public static function afficherErreur(string $messageErreur): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimerUtilisateur() : void {
        $user = $_GET['login'];
        if(ConnexionUtilisateur::estUtilisateur($user)){
            (new UtilisateurRepository())->supprimer($user);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur Supprimé", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
        }else{
            self::afficherErreur("Impossible de supprimer un utilisateur qui n'est pas vous.");
        }

    }

    public static function mettreAJour() : void {
        $repoUtilisateur = new UtilisateurRepository();
        $utilisateur = $repoUtilisateur->recupererParClePrimaire(rawurldecode($_GET["login"]));
        if(!MotDePasse::verifier($_GET["mdpOld"], $utilisateur->getMdpHache())){
            self::afficherErreur("Mot de passe incorrect.");
        }
        elseif (!($_GET["mdp"]==$_GET["mdp2"])){
            self::afficherErreur("Les deux mots de passe ne correspondent pas.");
        }
         else {
            $utilisateur = self::construireDepuisFormulaire($_GET);
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur mis à jour", "utilisateur" => $utilisateur->getLogin(), "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);

        }

    }

    public static function afficherFormulaireMiseAJour() : void {
        $user =rawurldecode($_GET['login']);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        if(ConnexionUtilisateur::estUtilisateur($user)) {
            self::afficherVue('vueGenerale.php', ["titre" => "Mise à jour utilisateur", "utilisateur" => $user, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        } else {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
        }
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom'], MotDePasse::hacher($tableauDonneesFormulaire['mdp']),isset($tableauDonneesFormulaire['estAdmin']));
    }

    public static function afficherFormulaireConnexion() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }
    public static function connecter() : void {
        if(empty($_GET["login"]) || empty($_GET["mdp"])){
            self::afficherErreur("Login et/ou mot de passe manquant.");
        } else {

            $repoUtilisateur = new UtilisateurRepository();
            $utilisateur = $repoUtilisateur->recupererParClePrimaire($_GET["login"]);

            if ($utilisateur === null) {
                self::afficherErreur("Login incorrect.");
            }

            if (!MotDePasse::verifier($_GET["mdp"], $utilisateur->getMdpHache())) {
                self::afficherErreur("Mot de passe incorrect.");

            } else {
                ConnexionUtilisateur::connecter($utilisateur->getLogin());
                if(ConnexionUtilisateur::estConnecte()){
                    self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur connecté", "utilisateur" => $utilisateur, "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
                }else {
                    self::afficherErreur("Erreur de connexion.");
                }
            }
        }
    }

    public static function afficherDeconnexion() : void {
        self::afficherVue('vueGenerale.php',["titre" => "Connexion","cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);
        ConnexionUtilisateur::deconnecter();
    }
}