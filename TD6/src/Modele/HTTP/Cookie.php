<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{
        $valeurString = serialize($valeur);

        if ($dureeExpiration != null) {
            $expiration = time() + $dureeExpiration;
        } else {
            $expiration = 0;
        }

        setcookie($cle, $valeurString, $expiration);

    }
    public static function lire(string $cle): mixed{
        if(isset($_COOKIE[$cle])){
            return unserialize($_COOKIE[$cle]);
        }
        return null;
    }

    public static function contient(string $cle): bool{
        return array_key_exists($cle, $_COOKIE) || isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{
        unset($_COOKIE[$cle]);
        setcookie($cle, '', time() + 1);
    }
}
