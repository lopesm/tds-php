<?php
namespace App\Covoiturage\Modele\HTTP;

use App\Covoiturage\Configuration\ConfigurationSite;
use Exception;

class Session
{
    private static ?Session $instance = null;

    public static function getInstance(): Session
    {
        $instance = self::$instance;

        if ($instance === null) {
            $instance = new Session();
            $instance->verifierDerniereActivite();
        }
        return $instance;
    }

    /**
     * @throws Exception
     */
    private function __construct()
    {
        if (session_id() === '') { 
            if (session_start() === false) {
                throw new Exception("La session n'a pas réussi à démarrer.");
            }
        }
    }

    public function contient($nom): bool
    {
        return isset($_SESSION[$nom]);
    }

    public function enregistrer(string $nom, mixed $valeur): void
    {
        $_SESSION[$nom] = $valeur;
    }

    public static function lire(string $nom): mixed
    {
       if (isset($_SESSION[$nom])) {
           return $_SESSION[$nom];
       }else {
           return null;
       }
    }

    public function supprimer($nom): void
    {
        unset($_SESSION[$nom]);
    }

    public function detruire() : void
    {
        session_unset();
        session_destroy();
        Cookie::supprimer(session_name());
        Session::$instance = null;
    }

    public function verifierDerniereActivite()
    {
        if (isset($_SESSION['derniereActivite']) && (time() - $_SESSION['derniereActivite'] > (ConfigurationSite::getDureeExpirationSession()))) {
            session_unset();
        }

        $_SESSION['derniereActivite'] = time();
    }
}
