<?php

namespace App\Covoiturage\Lib;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        $session->enregistrer($cleConnexion,$loginUtilisateur);

    }

    public static function estConnecte(): bool
    {
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        return $session->contient($cleConnexion);
    }

    public static function deconnecter(): void
    {
        $cleConnexion = self::$cleConnexion;
        $session = Session::getInstance();
        $session->supprimer($cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return Session::getInstance()->contient(self::$cleConnexion) ? Session::getInstance()->lire(self::$cleConnexion) : null;
    }

    public static function estUtilisateur($login): bool{
        if(self::estConnecte()){
            return self::getLoginUtilisateurConnecte() === $login;
        }
        return false;
    }

    public static function estAdministrateur() : bool{
        // Vérifier si un utilisateur est connecté
        if (!self::estConnecte()) {
            return false;
        }

        // Récupérer le login de l'utilisateur connecté
        $loginUtilisateur = self::getLoginUtilisateurConnecte();
        if (!$loginUtilisateur) {
            return false;
        }
        // Utiliser le repository pour vérifier si l'utilisateur est un administrateur
        $repoUtilisateur = new UtilisateurRepository();
        $utilisateur = $repoUtilisateur->recupererParClePrimaire($loginUtilisateur);


        // Assumer que la méthode "estAdministrateur" est définie dans le modèle Utilisateur
        return $utilisateur !== null && $utilisateur->isEstAdmin();
    }
}
