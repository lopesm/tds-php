<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;

require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

if(isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}
else {
    if (PreferenceControleur::existe()){
        $controleur = PreferenceControleur::lire();
    }
    else $controleur = 'utilisateur';
}

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);

if(isset($_GET['action'])) {
    $action = $_GET['action'];
    if (in_array($action, get_class_methods($nomDeClasseControleur)) && class_exists($nomDeClasseControleur)) {
        $nomDeClasseControleur::$action();
    }
    else {
        ControleurUtilisateur::afficherErreur("L'action $action demandée n'existe pas ! ");
    }

}
else $nomDeClasseControleur::afficherListe();